class ArticleAttachmentsController < ApplicationController
  load_and_authorize_resource
  def download
    @attachment = ArticleAttachment.find params[:id]
    # send_file @attachment.file.path, disposition: 'attachment', x_sendfile: true
    redirect_to @attachment.file.url
  end
end
