class UserSessionsController < ApplicationController
  skip_before_filter :require_login, :except => [:destroy]
  def new
    current_user and redirect_to user_path(current_user)
    @user = User.new
  end

  def create
    respond_to do |format|
      @user = login(params[:email],params[:password])
      if @user && !@user.retired?
        format.html { redirect_back_or_to(user_path(@user), :notice => 'Login successful.') }
        format.xml { render :xml => @user, :status => :created, :location => @user }
      else
        logout
        format.html { flash[:alert] = "Login failed."; redirect_to root_path }
        format.xml { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    logout
    redirect_to(root_path, :alert => 'Logged out!')
  end
end

