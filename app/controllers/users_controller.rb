class UsersController < ApplicationController
  skip_before_filter :require_login, :only => [:new, :create]
  load_and_authorize_resource
  # GET /users
  # GET /users.json
  def index
    list_of_users = User.includes(:domain).text_search(params[:search])
    unless current_user.admin?
      list_of_users = list_of_users.active
    end
    @users = list_of_users.paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    @domains = Domain.list_domains

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  def vcard
    @user = User.find params[:id]
  end

  # GET /users/new
  # GET /users/new.json
  def new
    unless current_user.admin?
      redirect_to user_path(current_user), alert: "Not Authorized"
      return
    end
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    @user.date_of_birth = @user.date_of_birth.strftime("%d/%m/%Y") if @user.date_of_birth
    unless current_user.admin? or current_user == @user
      redirect_to user_path(current_user), alert: "Not Authorized"
      return
    end
  end

  # POST /users
  # POST /users.json
  def create
    unless current_user.admin?
      redirect_to user_path(current_user), alert: "Not Authorized"
      return
    end

    @user = User.new(params[:user])
    if @user.save
      redirect_to user_path(current_user), notice: "Account has been created."
    else
      render action: "new" 
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    unless current_user.admin?
      params[:user].delete :role
      params[:user].delete :domain
    end


    @user = User.find(params[:id])
    if params[:user][:password].blank?
      params[:user].delete :password
      params[:user].delete :password_confirmation
      @user.password_not_required = true
    end

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  def retire
    @user = User.find params[:id]
    if @user.retire!
      redirect_to users_path, notice: "The user was retired"
    else
      redirect_to users_path, alert: "The user could not be retired"
    end
  end

  def revoke_retirement
    @user = User.find params[:id]
    if @user.revoke_retirement!
      redirect_to users_path, notice: "The user's retirement was revoked"
    else
      redirect_to users_path, alert: "The user's retirement could not be revoked"
    end
  end
end
