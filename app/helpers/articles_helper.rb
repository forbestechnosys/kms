module ArticlesHelper
  def render_article_action_buttons(article, css_var)
    rejection_modal = render_rejection_modal article               
    approval = publish = reject = refactor = delete = edit = buttons = ""
    approval = link_to("Send for Approval", send_for_approval_article_path(article), method: "post", class: css_var) if can? :send_for_approval, article

    publish = link_to("Publish", publish_article_path(article), method: "post", class: css_var) if can? :publish, article

    reject = link_to("Re-work", "#rejection_modal", class: css_var, role: "button", data: { toggle: "modal"}) if can? :reject, article

    refactor = link_to("Re-factor", refactor_article_path(article), method: "post", class: css_var) if can? :refactor, article

    delete = link_to 'Delete', article_path(article), method: :delete, data: { confirm: 'Are you sure? Once deleted, an article can never be restored' }, class: css_var + " btn-danger" if can? :destroy, article

    edit = link_to 'Edit', edit_article_path(article), class: css_var if can? :edit, article

    case article.aasm_state
    when 'draft'
      buttons = approval
    when 'approval_pending'
      buttons = publish  + reject + rejection_modal + "<br>".html_safe
    when 'rejected'
      buttons = refactor
    end

    return edit + buttons + delete
  end

  def render_article_tags article
    article.tags.pluck(:tag_name).collect do |tag_name|
      "<span class='label'>" + tag_name + "</span>" 
    end.join(' ').html_safe
  end

  def render_article_attachments article
    article.article_attachments.collect do |attachment|
      file_name = attachment.file_name
      extension = file_name.split('.').last
      case extension.downcase
      when 'xls', 'xlsx', 'ods'
        image = image_tag 'excel.png', width: 50, height: 50
      when 'doc', 'docx'
        image = image_tag 'word.png', width: 50, height: 50
      when 'ppt', 'pptx'
        image = image_tag 'ppt.png', width: 50, height: 50
      when 'png', 'jpg', 'jpeg', 'bmp'
        image = image_tag 'image.png', width: 50, height: 50
      when 'pdf'
        image = image_tag 'pdf.png', width: 50, height: 50
      when 'txt'
        image = image_tag 'txt.png', width: 50, height: 50
      when 'mov', 'avi', 'mpg', 'mpeg', 'mp4', '3gp', 'wmv', 'mkv', 'aaf', 'flv'
        image = image_tag 'video.png', width: 50, height: 50
      else
        image = image_tag 'unknown.png', width: 50, height: 50
      end
      content_tag('div', class: 'span3 center', style: "word-wrap: break-word; padding: 1em;") do
        link_to(image + '<br>'.html_safe + truncate(file_name,length: 18), "/uploads/#{attachment.id}/#{File.basename(attachment.file.url)}") + "<br>".html_safe + content_tag('b', "(#{attachment.file_size.to_i/1000}kB)", class: "muted")
      end
    end.join('').html_safe
  end

  def like_button article
    if cannot?(:like, article) or current_user.voted_as_when_voted_for(article)
      link_to(" Like", "#", class: "icon-thumbs-up muted", disabled: true)
    else
      link_to(" Like", like_article_path(article), class: "icon-thumbs-up", method: :post)
    end
  end

  def article_folder_path parent
    folder_array = []
    folder_array.append link_to(' Home', articles_path, remote: true, class: "icon-folder-open icon-large ajax_page")
    if parent
      folder_array += parent.ancestors.collect do |folder|
        link_to(folder.domain_name, articles_path(domain_id: folder.id), remote: true)
      end
      folder_array.append link_to(parent.domain_name, articles_path(domain_id: parent.id), remote: true, class: "ajax_page")
    end

    folder_array.join(" / ").html_safe
  end

  def render_rejection_modal article
    rejection_modal = "<div id='rejection_modal' class='modal hide fade' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>" +

      content_tag("div", class: "modal-header") do
      "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>".html_safe +
      content_tag("h3", "Re-work") 
      end +
        form_tag(reject_article_path(article)) do

        content_tag("div", class: "modal-body") do
          content_tag("p", "Please specify a valid reason for asking the author to re-work on this article") +
            text_area_tag(:rejection_reason, nil, rows: 6, maxlength: 500, class: "span10")
        end +

          content_tag("div", class: "modal-footer") do
          submit_tag 'Submit', class: "btn"
          end
        end + "</div>"
    return rejection_modal.html_safe
  end
end
