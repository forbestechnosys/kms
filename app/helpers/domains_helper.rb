module DomainsHelper
  def folder_path parent
    folder_array = []
    folder_array.append link_to(' Home', domains_path, remote: true, class: "icon-folder-open icon-large")
    if parent
      folder_array += parent.ancestors.collect do |folder|
        link_to(folder.domain_name, list_folders_domain_path(folder), remote: true)
      end
      folder_array.append link_to(parent.domain_name, list_folders_domain_path(parent), remote: true)
    end

    folder_array.join(" / ").html_safe
  end

  def welcome_message domain
    if domain.banner?
      render 'domains/welcome_with_banner', domain: domain
    else
      render 'domains/welcome_standard', domain: domain
    end
  end
end
