# == Schema Information
#
# Table name: article_tags
#
#  id         :integer          not null, primary key
#  article_id :integer
#  tag_id     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ArticleTag < ActiveRecord::Base
  attr_accessible :article_id, :tag_id
  belongs_to :article, touch: true
  belongs_to :tag, touch: true
end
