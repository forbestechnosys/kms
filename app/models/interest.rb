# == Schema Information
#
# Table name: interests
#
#  id            :integer          not null, primary key
#  interest_name :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  user_id       :integer
#

class Interest < ActiveRecord::Base
  attr_accessible :interest_name, :user_id

  belongs_to :user
end
