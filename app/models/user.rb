# == Schema Information
#
# Table name: users
#
#  id               :integer          not null, primary key
#  username         :string(255)      not null
#  email            :string(255)
#  crypted_password :string(255)
#  salt             :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  role             :string(255)
#  domain           :string(255)
#  about_me         :text
#  designation      :string(255)
#  avatar           :string(255)
#  mobile           :integer
#  state            :string(255)
#

require 'file_size_validator' 
class User < ActiveRecord::Base
  authenticates_with_sorcery!
  acts_as_voter

  attr_accessor :password_not_required
  attr_accessible :username, :email, :password, :password_confirmation, :role, :about_me, :designation, :skills_attributes, :interests_attributes, :avatar, :state, :mobile, :upload_limit, :domain_id, :date_of_birth, :remove_avatar
  validates_length_of :password, :minimum => 5, :message => "password must be at least 5 characters long", :if => :password
  validates_confirmation_of :password, :message => "should match confirmation", :if => :password
  validates_uniqueness_of :email, case_sensitive: false
  validates_presence_of :username, :email, :domain_id, :role
  validates_presence_of :password, :unless => :password_not_required
  validate :dob_validation

  has_many :articles
  has_many :skills
  has_many :interests
  belongs_to :domain

  accepts_nested_attributes_for :skills, :allow_destroy => true,  :reject_if => proc { |attributes| attributes['skill_name'].blank? }
  accepts_nested_attributes_for :interests, :allow_destroy => true,  :reject_if => proc { |attributes| attributes['interest_name'].blank? }

  mount_uploader :avatar, AvatarUploader
  validates :avatar, 
    :file_size => { 
    :maximum => 1.megabytes.to_i 
  } 

  before_validation :strip_whitespaces

  include PgSearch
  pg_search_scope :search, against: [[:username, 'A'], [:domain, 'A'], [:email, 'B'], [:designation, 'B'], [:state, 'B']],
    using: {
      tsearch: {
        dictionary: "english",
        prefix: true,
      }
    },
    associated_against: {
      skills: {:skill_name => 'B'},
    }

  def self.roles
    [:contributor, :coordinator, :admin, :domain_head]
  end

  def admin?
    role == 'admin'
  end

  def list_of_interests
    interests.pluck(:interest_name).join(", ")
  end

  def list_of_skills
    skills.pluck(:skill_name).join(", ")
  end

  def published_articles
    articles.where(aasm_state: 'published')
  end

  def draft_articles
    articles.where(aasm_state: 'draft')
  end

  def approval_pending
    articles.where(aasm_state: 'approval_pending')
  end

  def rejected_articles
    articles.where(aasm_state: 'rejected')
  end

  def km_coordinator_pending_approvals
    Article.includes(:domain).
      km_coordinator_pending_approvals(self)
  end

  def strip_whitespaces
    self.username = self.username.strip
    self.email = self.email.strip
  end

  def self.text_search(query)
    if query.present?
      search(query)
    else
      scoped.order('username asc')
    end
  end

  def self.active
    where("retire is not true")
  end

  def retire!
    update_attribute(:retire, true)
  end

  def revoke_retirement!
    update_attribute(:retire, false)
  end

  def retired?
    retire
  end

  def user_upload_limit
    @upload_limit ||= calculate_upload_limit
  end

  def calculate_upload_limit
    if upload_limit.to_f > 0.0
      the_upload_limit = self.upload_limit.to_f
    else
      the_upload_limit = 10
    end
  end

  private
  def dob_validation
    if self.date_of_birth
      dob = self.date_of_birth.to_date
      if (dob + 18.years) > Date.today
        errors.add :date_of_birth, "You need to be atleast 18 years old. Kindly enter a valid date of birth."
      elsif ((Date.today - dob)/365).to_i > 100
        errors.add :date_of_birth, "Are you really over a 100 years old? Kindly enter a valid date of birth."
      end
    end
  end

end
