# == Schema Information
#
# Table name: articles
#
#  id          :integer          not null, primary key
#  subject     :string(255)
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  aasm_state  :string(255)
#  user_id     :integer
#  domain_id   :integer
#

class Article < ActiveRecord::Base
  acts_as_votable
  include AASM
  belongs_to :user, touch: true
  belongs_to :approver, class_name: "User", foreign_key: :approver_id, touch: true
  belongs_to :domain, touch: true
  has_many :article_attachments
  has_many :article_tags
  has_many :tags, through: :article_tags
  attr_accessible :description, :subject, :user_id, :domain_id, :tag_ids, :article_attachments_attributes, :rejection_reason, :approver_id
  accepts_nested_attributes_for :article_attachments, :allow_destroy => true,  :reject_if => proc { |attributes| attributes['file'].blank? }

  validates_presence_of :subject
  validates_presence_of :domain_id, message: "Selection of domain folder is mandatory"
  validates :subject, :length => { :maximum => 200 }
  validates :description, :length => { :maximum => 20000 }
  validates :rejection_reason, :length => { :in => 6..500 }, allow_blank: true

  scope :pending_approval, 
    where(aasm_state: 'approval_pending')


  include PgSearch
  pg_search_scope :search, against: [[:subject, 'A'], [:description, 'C']],
    using: {
      tsearch: {
        dictionary: "english",
        prefix: true,
      }
    },
    associated_against: {
      user: {:username => 'A'},
      tags: {:tag_name => 'B'},
      domain: {:domain_name => 'A'},
      article_attachments: {:file_name => 'D'},
    }

  aasm do
    state :draft, initial: true, before_enter: :nullify_rejection_reason
    state :approval_pending
    state :published
    state :rejected

    event :refactor do
      transitions from: :rejected, to: :draft
    end

    event :reject do
      transitions from: :approval_pending, to: :rejected, guard: :rejection_reason_present?
    end

    event :send_for_approval do
      transitions from: :draft, to: :approval_pending
    end

    event :publish do
      transitions from: :approval_pending, to: :published, before_enter: :nullify_rejection_reason

    end

    event :unpublish do
      transitions from: :published, to: :draft
    end
  end

  def nullify_rejection_reason
    rejection_reason = nil
  end

  def rejection_reason_present?
    !rejection_reason.blank?
  end

  def published_by?
    user
  end

  def self.text_search(query)
    if query.present?
      search(query)
    else
      scoped.order('updated_at desc')
    end
  end

  def self.filter_by_domain(id)
    articles ||= where(aasm_state: 'published').belongs_to_domain_or_its_subfolders(id)
    articles
  end

  def self.belongs_to_domain_or_its_subfolders id
    domain = Domain.find id
    descendants = domain.descendants
    ids = descendants.pluck(:id) << id
    where("domain_id in (?)", ids)  
  end

  def self.filter_for_feed(params, user)
    articles = self.includes(:domain).
      includes(:user)

    if params[:km_pending] #KM coordinator's pending actions
       return articles.
        km_coordinator_pending_approvals(user).
        paginate(:page => params[:page], :per_page => 10)

    elsif params[:state].present? #Current user's states
       articles = articles.
        where(user_id: user.id).
        where(aasm_state: params[:state])

    elsif params[:domain_id].present? #Filter for domain
       articles = articles.
        filter_by_domain(params[:domain_id])

    else #Everything
       articles = articles.
        where(aasm_state: 'published')
    end

    articles.text_search(params[:search]).
      paginate(:page => params[:page], :per_page => 10)
  end

  def self.km_coordinator_pending_approvals user
    pending_approval.reject do |article|
      user.domain != article.domain.parent_domain
    end
  end

  def self.feed_header params
    if params[:km_pending] #KM coordinator's pending actions
      return "<h3 class='icon-check'>&nbsp; These Articles have to be vetted by you</h3>"

    elsif params[:state].present? #Current user's states
      case params[:state]
      when "draft"
        return "<h3 class='icon-file-alt'>&nbsp;Your Draft Articles</h3>"
      when "approval_pending"
        return "<h3 class='icon-list'>&nbsp;A KM-Co-ordinator has to approve these Articles</h3>"
      when "published"
        return "<h3 class='icon-globe'>&nbsp;Your Published Articles</h3>"
      when "rejected"
        return "<h3 class='icon-wrench'>&nbsp;You need to work on these Articles again</h3>"
      end
      return

    elsif params[:domain_id].present? #Filter for domain
      return nil

    else #Everything
      return "<h3 class='icon-globe'>&nbsp; Public Feed</h3>"
    end
  end

end
