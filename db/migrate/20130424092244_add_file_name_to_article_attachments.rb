class AddFileNameToArticleAttachments < ActiveRecord::Migration
  def change
    add_column :article_attachments, :file_name, :string
  end
end
