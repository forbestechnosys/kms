class AddIndexesToSpeedUpUserSearch < ActiveRecord::Migration
  def up
    execute "create index users_username on users using gin(to_tsvector('english', username))"
    execute "create index users_email on users using gin(to_tsvector('english', email))"
    execute "create index users_domain on users using gin(to_tsvector('english', domain))"
    execute "create index users_designation on users using gin(to_tsvector('english', designation))"
    execute "create index users_state on users using gin(to_tsvector('english', state))"
    execute "create index skills_skill_name on skills using gin(to_tsvector('english', skill_name))"
  end

  def down
    execute "drop index users_username"
    execute "drop index users_email"
    execute "drop index users_domain"
    execute "drop index users_designation"
    execute "drop index users_state"
    execute "drop index skills_skill_name"
  end
end
