class AddIndexAasmStateToArticles < ActiveRecord::Migration
  def change
    add_index :articles, :aasm_state
  end
end
