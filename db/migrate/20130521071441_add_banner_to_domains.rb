class AddBannerToDomains < ActiveRecord::Migration
  def change
    add_column :domains, :banner, :string
  end
end
