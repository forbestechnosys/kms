class AddUserAndDomainIndexToArticles < ActiveRecord::Migration
  def self.up
    add_index :articles, :user_id
    add_index :articles, :domain_id
  end

  def self.down
    remove_index :articles, :user_id
    remove_index :articles, :domain_id
  end
end
