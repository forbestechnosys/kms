class CreateArticleAttachments < ActiveRecord::Migration
  def change
    create_table :article_attachments do |t|
      t.integer :article_id
      t.string :file

      t.timestamps
    end
  end
end
