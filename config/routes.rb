Kms::Application.routes.draw do

  resources :notifications do
    member do
      get 'feed'
    end
  end
  resources :tags


  get "article_attachments/download"

  resources :domains do
    member do
      get 'list_folders'
    end
  end


  get "pages/home"
  root :to => 'pages#home'

  resources :user_sessions, as: :user_sessions
  resources :users, as: :users do
    member do
      get 'vcard'
      put 'retire'
      put 'revoke_retirement'
    end
  end
  resources :articles do
    member do
      post 'publish'
      post 'send_for_approval'
      post 'unpublish'
      post 'reject'
      post 'refactor'
      post 'like'
    end
  end
    
  #secure file downloads
  match "/uploads/:id/:basename.:extension", :controller => "article_attachments", :action => "download", :conditions => { :method => :get }
  match 'login' => 'user_sessions#new', :as => :login
  match 'logout' => 'user_sessions#destroy', :as => :logout
end
